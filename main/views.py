# coding=utf-8
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, RedirectView, CreateView, DetailView
from django.shortcuts import get_object_or_404

from .models import Blog, Post


class IndexView(ListView, LoginRequiredMixin):
    model = Blog
    template_name = 'main/index.html'


class SubscribeView(RedirectView):
    url = '/'
    
    def get_redirect_url(self, *args, **kwargs):
        blog = get_object_or_404(Blog, pk=kwargs['pk'])
        b = self.request.user.blog
        b.subscribe.add(blog)
        b.save()
        return super(SubscribeView, self).get_redirect_url(*args, **kwargs)


class UnsubscribeView(RedirectView, LoginRequiredMixin):
    url = '/'
    
    def get_redirect_url(self, *args, **kwargs):
        blog = get_object_or_404(Blog, pk=kwargs['pk'])
        b = self.request.user.blog
        b.subscribe.remove(blog)
        if b.readed:
            readed = [int(x) for x in b.readed.split(',')]
            for p in blog.posts.filter(pk__in=readed):
                del readed[readed.index(p.pk)]
            b.readed = ','.join([str(x) for x in readed])
        b.save()
        return super(UnsubscribeView, self).get_redirect_url(*args, **kwargs)


class FeedView(ListView, LoginRequiredMixin):
    model = Post
    template_name = 'main/feed.html'
    
    def get_context_data(self, **kwargs):
        context = super(FeedView, self).get_context_data(**kwargs)
        readed = self.request.user.blog.readed
        if readed:
            context['readed'] = [int(x) for x in readed.split(',')]
        else:
            context['readed'] = []
        return context
    
    def get_queryset(self, *args, **kwargs):
        return Post.objects.filter(blog__in=self.request.user.blog.subscribe.all()).order_by('-cr_time')


class ReadedView(RedirectView, LoginRequiredMixin):
    url = '/feed/'
    
    def get_redirect_url(self, *args, **kwargs):
        post = get_object_or_404(Post, pk=kwargs['pk'])
        b = self.request.user.blog
        if b.readed:
            b.readed += ',%d' % post.pk
        else:
            b.readed = str(post.pk)
        b.save()
        return super(ReadedView, self).get_redirect_url(*args, **kwargs)


class MyBlogView(ListView, LoginRequiredMixin):
    model = Post
    template_name = 'main/my_blog.html'

    def get_queryset(self, *args, **kwargs):
        return Post.objects.filter(blog__author=self.request.user)


class NewPostView(CreateView, LoginRequiredMixin):
    model = Post
    fields = ['title', 'text']
    template_name = 'main/new_post.html'
    success_url = '/my_blog/'
    
    def form_valid(self, form):
        form.instance.blog = self.request.user.blog
        return super(NewPostView, self).form_valid(form)


class PostView(DetailView, LoginRequiredMixin):
    model = Post
    template_name = 'main/post.html'