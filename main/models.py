# coding=utf-8
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.mail import send_mass_mail

from sb.settings import CURRENT_DOMAIN as cd


class Blog(models.Model):
    author = models.OneToOneField(User)
    subscribe = models.ManyToManyField('self', symmetrical=False, related_name='subscribers', null=True, blank=True)
    readed = models.TextField(u'Прочитанные посты', default='', blank=True)
    cr_time = models.DateTimeField(u'Время создания блога', auto_now_add=True)

    class Meta:
        verbose_name = 'Блог'
        verbose_name_plural = 'Блоги'

    def __str__(self):
        return u'Блог пользователя %s' % self.author.get_full_name()


class Post(models.Model):
    blog = models.ForeignKey(Blog, related_name='posts')
    title = models.CharField(u'Название', max_length=200)
    text = models.TextField(u'Текст')
    cr_time = models.DateTimeField(u'Время создания поста', auto_now_add=True)

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):
        return u'Пост пользователя %s' % self.blog.author.get_full_name()

def send_notify(sender, instance, **kwargs):
    messages = []
    for b in instance.blog.subscribers.all():
        messages.append((u'Новый пост', u'Добавлен новый пост в блоге, можно посмотреть по адресу http://%s/post/%d' % (cd, instance.id), 'info@simple_blog.ru', [b.author.email]))
    if messages:
        send_mass_mail(messages, fail_silently=False)
    
post_save.connect(send_notify, sender=Post)