from django.conf.urls import url
from .views import IndexView, SubscribeView, UnsubscribeView, FeedView, ReadedView, MyBlogView, NewPostView, PostView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^subscribe/(?P<pk>\d+)/$', SubscribeView.as_view(), name='subscribe'),
    url(r'^unsubscribe/(?P<pk>\d+)/$', UnsubscribeView.as_view(), name='unsubscribe'),
    url(r'^feed/$', FeedView.as_view(), name='feed'),
    url(r'^readed/(?P<pk>\d+)/$', ReadedView.as_view(), name='readed'),
    url(r'^my_blog/$', MyBlogView.as_view(), name='my_blog'),
    url(r'^new_post/$', NewPostView.as_view(), name='new_post'),
    url(r'^post/(?P<pk>\d+)/$', PostView.as_view(), name='post'),
]