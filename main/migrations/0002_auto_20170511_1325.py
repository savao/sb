# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-11 13:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='readed',
            field=models.TextField(default='', verbose_name='Прочитанные посты'),
        ),
        migrations.AlterField(
            model_name='blog',
            name='subscribe',
            field=models.ManyToManyField(blank=True, null=True, related_name='_blog_subscribe_+', to='main.Blog'),
        ),
    ]
